package com.example.reactive;

import java.io.BufferedReader;
import java.util.Objects;
import java.util.concurrent.Callable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;

public class ResourceCleanupExercise {

  private final Scheduler ioScheduler;

  public ResourceCleanupExercise(Scheduler ioScheduler) {
    this.ioScheduler = Objects.requireNonNull(ioScheduler);
  }

  /**
   * Creates a {@code Flux} of lines prefixed with line number retrieved from a {@code
   * BufferedReader} provided by {@code bufferedReaderSupplier}. For example given the lines
   * produced by the buffered reader:
   * <pre>
   *     First line
   *     Second line
   *     Third line
   * </pre>
   * the returned {@code Flux} will emit:
   * <pre>
   *     1: First line
   *     2: Second line
   *     3: Third line
   * </pre>
   * <p>
   * The returned {@code Flux} closes the provided {@code bufferedReaderSupplier} upon termination.
   *
   * @throws ExerciseException signaled by returned {@code Flux} when {@code IOException} or {@code
   *                           UncheckedIOException} occurs internally.
   */
  public Flux<String> lineNumbers(Callable<BufferedReader> bufferedReaderSupplier) {
    // Hint: Flux.using(...)

    throw new UnsupportedOperationException("Not implemented!");
  }
}
