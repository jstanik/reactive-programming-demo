package com.example.reactive;

import java.util.stream.IntStream;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class TableOfContentExercise {

  private static final String SECTION_PREFIX = "SECTION ";
  private static final String PAGE_DELIMITER = "===";

  /**
   * Generates <em>Table of Content</em> for a {@code Flux} of lines. Lines containing section
   * titles start with prefix "SECTION ". Pages are separated by triple equal symbol "===". Example
   * of the input {@code Flux} emitting following lines:
   * <pre>
   *     SECTION 1 First section
   *     Free text of the first section
   *     SECTION 1.1 Subsection
   *     Free text of the subsection
   *     spanning multiple lines
   *     ===
   *     or even pages.
   *     SECTION 2 Second section
   *     Text of the second section
   *     which also can be very long
   *     ===
   *     SECTION 3 Third section
   *     This third section appears on the 3rd page.
   * </pre>
   * <p>
   * For the example input the method returns a {@code Mono} emitting the following <em>Table of
   * Content:</em>
   * <pre>
   *     1 First section ...................................... 1
   *     1.1 Subsection ....................................... 1
   *     2 Second section ..................................... 2
   *     3 Third section ...................................... 3
   * </pre>
   */
  public Mono<String> generateTableOfContent(Flux<String> lines) {

    // For formatting Table of Content line use
    // private String formatTableOfContentLine(String sectionLine, int pageNumber)
    // of this class
    //
    // Hint: windowWhile

    throw new UnsupportedOperationException("Not implemented!");
  }

  /**
   * Creates a line for <em>Table of Content</em> by extracting section name from the {@code
   * sectionLine}, appending dot padding up to 54 characters and then appending the page number.
   *
   * @param sectionLine s line containing section title
   * @param pageNumber  the page number the line is located at
   * @return a line for Table of Content.
   */
  private String formatTableOfContentLine(String sectionLine, int pageNumber) {

    int sectionPrefixLength = 0;
    if (sectionLine.startsWith(SECTION_PREFIX)) {
      sectionPrefixLength = SECTION_PREFIX.length();
    }

    IntStream section = sectionLine.chars()
        .skip(sectionPrefixLength)
        .limit(50);

    IntStream padding = IntStream.concat(IntStream.of(' '), IntStream.generate(() -> '.'));

    return IntStream.concat(section, padding)
        .limit(54)
        .mapToObj(c -> (char) c)
        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
        .append(' ')
        .append(pageNumber)
        .toString();
  }
}
