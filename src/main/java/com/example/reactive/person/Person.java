package com.example.reactive.person;

import java.util.List;
import java.util.Objects;
import reactor.core.publisher.Flux;

/**
 * Entity record representing a person.
 */
public record Person(
    PersonId id,
    String firstName,
    String lastName,
    List<PhoneNumber> phoneNumbers
) {

  public Person {
    Objects.requireNonNull(id);
    phoneNumbers = List.copyOf(phoneNumbers);
  }

  /**
   * Gets persons phone numbers.
   *
   * @return flux of phone numbers
   */
  public Flux<PhoneNumber> getPhoneNumbers() {
    return Flux.fromIterable(phoneNumbers);
  }
}