package com.example.reactive.person;

import java.util.Objects;

/**
 * A phone number.
 */
public record PhoneNumber(String value) {

  public PhoneNumber {
    Objects.requireNonNull(value);
  }

  /**
   * Creates a phone number from a string.
   *
   * @param number
   * @return a new instance of the phone number
   */
  public static PhoneNumber from(String number) {
    return new PhoneNumber(number);
  }

  /**
   * Gets a string representation of this phone number.
   *
   * @return the phone number as a string
   */
  public String toString() {
    return value;
  }
}
