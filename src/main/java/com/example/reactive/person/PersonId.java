package com.example.reactive.person;

/**
 * Identifier for the {@link Person} entity.
 */
public record PersonId(String value) {

  public static PersonId personId(String value) {
    return new PersonId(value);
  }

}
