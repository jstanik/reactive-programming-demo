package com.example.reactive.person;

import java.util.Optional;

public interface PersonRepository {

  /**
   * Finds a person by the identifier.
   *
   * @param id the identifier to search by
   * @return a person with the given {@code id} or an empty {@code Optional} if there is no person
   * with the given {@code id}.
   */
  Optional<Person> findById(PersonId id);
}
