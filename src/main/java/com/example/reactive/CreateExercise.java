package com.example.reactive;

import java.util.function.Supplier;
import reactor.core.publisher.Mono;

public class CreateExercise {

  /**
   * Creates a {@code Mono<String>} which just produces a given value.
   *
   * @param value the value to produce
   * @return {@code Mono<String>}
   */
  public Mono<String> fromValue(String value) {
    throw new UnsupportedOperationException("Not implemented!");
  }

  /**
   * Creates a {@code Mono<String>} which publishes value from the {@code supplier}.
   *
   * @param supplier the supplier of the value the returned {@code Mono<String>} should publish.
   * @return {@code Mono<String>}
   */
  public Mono<String> fromSupplier(Supplier<String> supplier) {
    throw new UnsupportedOperationException("Not implemented!");
  }

  /**
   * Creates a {@code Mono<String>} which publishes a value from {@code Mono<String>} which will be returned by the supplier when called.
   */
  public Mono<String> fromMonoSupplier(Supplier<Mono<String>> monoSupplier) {
    throw new UnsupportedOperationException("Not implemented!");
  }

  /**
   * Creates a {@code Mono<String>} which only emits {@code error} signal.
   *
   * @param throwable the throwable to emit
   * @return {@code Mono<String>}
   */
  public Mono<String> createError(Throwable throwable) {
    throw new UnsupportedOperationException("Not implemented!");
  }
}
