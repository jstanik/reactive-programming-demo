package com.example.reactive;

public class ImperativeVsDeclarative {

  public static void main(String[] args) {
    System.out.println("Declarative: " + declarative());
    System.out.println("Imperative: " + imperative());
  }

  public static String declarative() {
    // TODO
    return "";
  }

  public static String imperative() {
    // TODO
    return "";
  }
}
