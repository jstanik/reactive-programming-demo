package com.example.reactive;

/**
 * Thrown when needed by an exercise.
 */
public class ExerciseException extends RuntimeException {

  public ExerciseException(String message) {
    super(message);
  }

  public ExerciseException(String message, Throwable cause) {
    super(message, cause);
  }

  public ExerciseException(Throwable cause) {
    super(cause);
  }
}
