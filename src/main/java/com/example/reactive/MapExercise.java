package com.example.reactive;

import com.example.reactive.person.PersonId;
import com.example.reactive.person.PersonRepository;
import com.example.reactive.person.PhoneNumber;
import java.util.Objects;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class MapExercise {

  private final PersonRepository personRepository;

  MapExercise(PersonRepository personRepository) {
    this.personRepository = Objects.requireNonNull(personRepository);
  }

  /**
   * Gets phone numbers for a person identified by its identifier.
   *
   * @param personId a {@code Mono} emitting a person identifier
   * @return a {@code Flux} of person's phone numbers.
   * @throws IllegalArgumentException emitted by the returned {@code Flux} if the {@code personId}
   *                                  is an empty {@code Mono}
   * @throws IllegalStateException    emitted by the returned {@code Flux} if there is no person for
   *                                  the provided {@code personId}
   */
  public Flux<PhoneNumber> getPhoneNumbers(Mono<PersonId> personId) {
    // Use `personRepository` to find the person by `personId`
    // Hint: switchIfEmpty, flatMapMany

    throw new UnsupportedOperationException("Not implemented!");
  }
}
