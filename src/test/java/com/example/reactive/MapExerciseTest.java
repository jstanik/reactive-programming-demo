package com.example.reactive;

import static com.example.reactive.person.PersonId.personId;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.example.reactive.person.Person;
import com.example.reactive.person.PersonId;
import com.example.reactive.person.PersonRepository;
import com.example.reactive.person.PhoneNumber;
import java.util.Arrays;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.TestMonoJust;
import reactor.test.StepVerifier;

class MapExerciseTest {

  private PersonRepository personRepository;

  private MapExercise cut;

  @BeforeEach
  void beforeEach() {
    personRepository = mock(PersonRepository.class);
    cut = new MapExercise(personRepository);
  }

  /**
   * Tests that subscription is not created inside the tested method.
   */
  @Test
  void getPhoneNumbers_01() {
    TestMonoJust<PersonId> mono = new TestMonoJust<>(personId("0"));
    cut.getPhoneNumbers(mono);

    assertFalse(mono.subscribeCalled(), "Subscription called inside tested method.");
  }

  /**
   * Tests that IllegalArgumentException is thrown if the input mono is empty
   */
  @Test
  void getPhoneNumbers_02() {
    Flux<PhoneNumber> result = cut.getPhoneNumbers(Mono.empty());

    StepVerifier.create(result)
        .verifyError(IllegalArgumentException.class);
  }

  /**
   * Tests that IllegalStateExcetpion is thrown if for the input ID no person is found
   */
  @Test
  void getPhoneNumbers_03() {
    when(personRepository.findById(any())).thenReturn(Optional.empty());

    Flux<PhoneNumber> result = cut.getPhoneNumbers(Mono.just(personId("0")));

    StepVerifier.create(result)
        .verifyError(IllegalStateException.class);
  }

  /**
   * Tests the successful case
   */
  @Test
  void getPhoneNumbers_04() {
    PhoneNumber pn1 = PhoneNumber.from("1");
    PhoneNumber pn2 = PhoneNumber.from("2");
    Person p = new Person(personId("1"), "John", "Smith",
        Arrays.asList(pn1, pn2));

    when(personRepository.findById(any())).thenReturn(Optional.of(p));

    Flux<PhoneNumber> result = cut.getPhoneNumbers(Mono.just(personId("0")));

    StepVerifier.create(result)
        .expectNext(pn1, pn2)
        .verifyComplete();
  }
}
