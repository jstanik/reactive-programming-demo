package com.example.reactive;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import reactor.test.StepVerifier;

class ResourceCleanupExerciseTest {

  private ResourceCleanupExercise cut;

  private AtomicBoolean schedulerUsed = new AtomicBoolean();

  private Scheduler ioScheduler = new Scheduler() {
    private Scheduler target = Schedulers.boundedElastic();

    @Override
    public Disposable schedule(Runnable task) {
      schedulerUsed.set(true);
      return target.schedule(task);
    }

    @Override
    public Worker createWorker() {
      schedulerUsed.set(true);
      return target.createWorker();
    }
  };

  @BeforeEach
  void beforeEach() {
    cut = new ResourceCleanupExercise(ioScheduler);
  }

  @Test
  void testLineNumbers() throws Exception {
    TestBufferedReader reader = new TestBufferedReader(new StringReader("Hello\nFlux!"));
    Flux<String> result = cut.lineNumbers(() -> reader);

    assertFalse(reader.isClosed(), "Reader closed in the tested method before any subscription");

    StepVerifier.create(result)
        .expectNext("1: Hello", "2: Flux!")
        .verifyComplete();

    assertTrue(reader.isClosed(), "Reader not closed by the returned Flux.");
    assertTrue(schedulerUsed.get(), "Dedicated scheduler not used.");
  }

  @Test
  void testLineNumbers_IOException_wrapped_on_close() throws Exception {
    TestBufferedReader reader = new TestBufferedReader(new StringReader("Hello\nFlux!")) {
      @Override
      public void close() throws IOException {
        throw new IOException();
      }
    };
    Flux<String> result = cut.lineNumbers(() -> reader);

    assertFalse(reader.isClosed(), "Reader closed in the tested method before any subscription");

    StepVerifier.create(result)
        .expectNext("1: Hello", "2: Flux!")
        .verifyError(ExerciseException.class);
  }

  @Test
  void testLineNumbers_IOException_wrapped_on_readLine() throws Exception {
    TestBufferedReader reader = new TestBufferedReader(new StringReader("Hello\nFlux!")) {
      @Override
      public String readLine() throws IOException {
        throw new IOException();
      }
    };
    Flux<String> result = cut.lineNumbers(() -> reader);

    StepVerifier.create(result)
        .verifyError(ExerciseException.class);
  }

  private static class TestBufferedReader extends BufferedReader {

    private volatile boolean closed;

    public TestBufferedReader(Reader in) {
      super(in);
    }

    @Override
    public void close() throws IOException {
      super.close();
      closed = true;
    }

    public boolean isClosed() {
      return closed;
    }
  }
}