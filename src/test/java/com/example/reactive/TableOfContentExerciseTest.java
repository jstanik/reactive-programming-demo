package com.example.reactive;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.core.publisher.TestFluxArray;
import reactor.test.StepVerifier;

class TableOfContentExerciseTest {

  private TableOfContentExercise cut = new TableOfContentExercise();

  @Test
  void generateTableOfContent() {
    String[] lines = {
        "SECTION 1 Title",
        "Introduction to the topic.",
        "SECTION 1.1 SubTitle A",
        "Text for the section A",
        "SECTION 1.2 SubTitle B",
        "text for the section B",
        "===",
        "SECTION 1.2.1 SubTitle C",
        "text for the section C",
        "SECTION 2 Title D",
        "text for section D",
        "===",
        "SECTION 2.1 Subtitle E",
        "SECTION 2.2 Very long name of the section exceeding 50 characters."
    };

    String expectedOutput = "1 Title .............................................. 1\n"
        + "1.1 SubTitle A ....................................... 1\n"
        + "1.2 SubTitle B ....................................... 1\n"
        + "1.2.1 SubTitle C ..................................... 2\n"
        + "2 Title D ............................................ 2\n"
        + "2.1 Subtitle E ....................................... 3\n"
        + "2.2 Very long name of the section exceeding 50 cha ... 3";

    TestFluxArray<String> input = new TestFluxArray<>(lines);
    Mono<String> result = cut.generateTableOfContent(input)
        .doOnSuccess(System.out::println);

    assertFalse(input.subscribeCalled(), "Subscribed to input in tested method.");

    StepVerifier.create(result)
        .expectNext(expectedOutput)
        .verifyComplete();
  }


}