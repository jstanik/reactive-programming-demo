package com.example.reactive;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

class CreateExerciseTest {

  private static final String EXPECTED = "VALUE";
  private CreateExercise cut = new CreateExercise();

  @Test
  void fromValue() {
    StepVerifier.create(cut.fromValue(EXPECTED))
        .expectNext(EXPECTED)
        .verifyComplete();
  }

  @Test
  void fromSupplier() {
    AtomicBoolean called = new AtomicBoolean();
    Supplier<String> supplier = () -> {
      called.set(true);
      return EXPECTED;
    };

    Mono<String> result = cut.fromSupplier(supplier);

    assertFalse(called.get(), "Supplier called in the 'fromSupplier' method.");

    StepVerifier.create(result)
        .expectNext(EXPECTED)
        .verifyComplete();
  }

  @Test
  void fromMonoSupplier() {
    AtomicBoolean called = new AtomicBoolean();

    Mono<String> result = cut.fromMonoSupplier(() -> {
      called.set(true);
      return Mono.just(EXPECTED);
    });

    assertFalse(called.get(), "Supplier called in the 'fromMonoSupplier' method.");

    StepVerifier.create(result)
        .expectNext(EXPECTED)
        .verifyComplete();
  }

  @Test
  void createError() {
    IllegalStateException error = new IllegalStateException("ERROR");
    StepVerifier.create(cut.createError(error))
        .verifyErrorMatches(e -> e == error);
  }
}