package reactor.core.publisher;

import java.time.Duration;
import java.util.Objects;

import java.util.concurrent.atomic.AtomicBoolean;
import reactor.core.CoreSubscriber;
import reactor.core.Fuseable;

/*
 * Copied from reactor.core.publisher.MonoJust for demonstration purposes only.
 */
public final class TestMonoJust<T>
    extends Mono<T>
    implements Fuseable.ScalarCallable<T>, Fuseable, SourceProducer<T> {

  final T value;

  private AtomicBoolean subscribeCalled = new AtomicBoolean();

  public TestMonoJust(T value) {
    this.value = Objects.requireNonNull(value, "value");
  }

  public boolean subscribeCalled() {
    return subscribeCalled.get();
  }

  @Override
  public T call() throws Exception {
    return value;
  }

  @Override
  public T block(Duration m) {
    return value;
  }

  @Override
  public T block() {
    return value;
  }

  @Override
  public void subscribe(CoreSubscriber<? super T> actual) {
    subscribeCalled.set(true);
    actual.onSubscribe(Operators.scalarSubscription(actual, value));
  }

  @Override
  public Object scanUnsafe(Attr key) {
    if (key == Attr.BUFFERED) {
      return 1;
    }
    return null;
  }
}
